package ru.tsc.pavlov.tm.exception.empty;

import ru.tsc.pavlov.tm.exception.AbstractException;

public class EmptyUserPasswordException extends AbstractException {

   public EmptyUserPasswordException(){
        super("Error. Received password is empty.");
    }

    public EmptyUserPasswordException(String value){
        super("Error. Received password for user " + value + " is empty.");
    }

}
