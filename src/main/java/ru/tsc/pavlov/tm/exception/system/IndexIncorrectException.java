package ru.tsc.pavlov.tm.exception.system;

import ru.tsc.pavlov.tm.exception.AbstractException;

public class IndexIncorrectException extends AbstractException {

    public IndexIncorrectException() {
        super("Error. Index is incorrect.");
    }

    public IndexIncorrectException(final String value) {
        super("Error. " + value + " is not a number.");
    }

}
