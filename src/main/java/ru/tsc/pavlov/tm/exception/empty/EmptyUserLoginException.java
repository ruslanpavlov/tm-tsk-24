package ru.tsc.pavlov.tm.exception.empty;

import ru.tsc.pavlov.tm.exception.AbstractException;

public class EmptyUserLoginException extends AbstractException {

    public EmptyUserLoginException() {
        super("Error. User Login is empty.");
    }

}
