package ru.tsc.pavlov.tm.command.mutually;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.pavlov.tm.constant.TerminalConst;
import ru.tsc.pavlov.tm.exception.empty.EmptyIdException;
import ru.tsc.pavlov.tm.util.TerminalUtil;

public class ProjectRemoveByIdCommand extends AbstractMutuallyCommand {

    @NotNull
    @Override
    public String getName() {
        return TerminalConst.PROJECT_REMOVE_BY_ID;
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Remove project by id";
    }

    @Override
    public void execute() {
        final String userId = getAuthService().getCurrentUserId();
        System.out.println("[ENTER PROJECT ID]");
        final String id = TerminalUtil.nextLine();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        getProjectTaskService().removeById(userId, id);
        System.out.println("[OK]");
    }

}
