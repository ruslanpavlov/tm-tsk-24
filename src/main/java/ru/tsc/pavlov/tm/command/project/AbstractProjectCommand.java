package ru.tsc.pavlov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.pavlov.tm.api.service.IAuthService;
import ru.tsc.pavlov.tm.api.service.IProjectService;
import ru.tsc.pavlov.tm.api.service.IUserService;
import ru.tsc.pavlov.tm.command.AbstractCommand;
import ru.tsc.pavlov.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.pavlov.tm.model.Project;

public abstract class AbstractProjectCommand extends AbstractCommand {

    @NotNull
    protected IProjectService getProjectService() {
        return serviceLocator.getProjectService();
    }

    @NotNull
    protected IAuthService getAuthService() { return serviceLocator.getAuthService(); }

    protected void showProject(@Nullable final Project project) {
        if (project == null) throw new ProjectNotFoundException();
        System.out.println("Id: " + project.getId());
        System.out.println("Name: " + project.getName());
        System.out.println("Description: " + project.getDescription());
        System.out.println("Status: " + project.getStatus().getDisplayName());
    }

}
