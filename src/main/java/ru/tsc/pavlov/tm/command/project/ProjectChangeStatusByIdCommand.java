package ru.tsc.pavlov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.pavlov.tm.constant.TerminalConst;
import ru.tsc.pavlov.tm.enumerated.Status;
import ru.tsc.pavlov.tm.enumerated.UserRole;
import ru.tsc.pavlov.tm.model.Project;
import ru.tsc.pavlov.tm.model.User;
import ru.tsc.pavlov.tm.util.TerminalUtil;

import java.util.Arrays;

public class ProjectChangeStatusByIdCommand extends AbstractProjectCommand {

    @Override
    public UserRole[] roles() {
        return UserRole.values();
    }

    @NotNull
    @Override
    public String getName() {
        return TerminalConst.PROJECT_CHANGE_STATUS_BY_ID;
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Change project status by id";
    }

    @Override
    public void execute() {
        @Nullable final String userId = getAuthService().getCurrentUserId();
        System.out.println("[ENTER ID]");
        @Nullable final String id = TerminalUtil.nextLine();
        System.out.println("[ENTER STATUS]");
        System.out.println((Arrays.toString(Status.values())));
        @Nullable final String statusValue = TerminalUtil.nextLine();
        @NotNull Status status = Status.valueOf(statusValue);
        @Nullable final Project project = getProjectService().changeStatusById(userId, id, status);
        if (project == null)
            System.out.println("Incorrect values");
    }

}
