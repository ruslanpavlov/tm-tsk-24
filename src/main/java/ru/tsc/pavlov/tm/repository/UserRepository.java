package ru.tsc.pavlov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.pavlov.tm.api.repository.IUserRepository;
import ru.tsc.pavlov.tm.model.User;

import java.util.ArrayList;
import java.util.List;

public class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @Nullable
    @Override
    public User findByLogin(@Nullable final String login) {
        return list.stream()
                .filter(u -> u.getLogin().toLowerCase().equals(login.toLowerCase()))
                .findFirst()
                .orElse(null);
    }

    @Nullable
    @Override
    public User findByEmail(@NotNull final String email) {
        return list.stream()
                .filter(u -> u.getEmail().toLowerCase().equals(email.toLowerCase()))
                .findFirst()
                .orElse(null);
    }

    @Nullable
    @Override
    public User removeByLogin(@NotNull final String login) {
        final User user = findByLogin(login);
        remove(user);
        return user;
    }

}
