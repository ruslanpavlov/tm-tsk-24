package ru.tsc.pavlov.tm.repository;

import org.jetbrains.annotations.NotNull;
import ru.tsc.pavlov.tm.api.repository.IOwnerRepository;
import ru.tsc.pavlov.tm.exception.AbstractException;
import ru.tsc.pavlov.tm.exception.entity.EntityNotFoundException;
import ru.tsc.pavlov.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.pavlov.tm.model.AbstractOwnerEntity;
import ru.tsc.pavlov.tm.model.Project;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public abstract class AbstractOwnerRepository <E extends AbstractOwnerEntity> extends AbstractRepository<E> implements IOwnerRepository<E> {

    @NotNull
    @Override
    public E add(@NotNull String userId, @NotNull E entity) {
        entity.setUserId(userId);
        list.add(entity);
        return entity;
    }

    @Override
    public void remove(@NotNull final String userId, @NotNull final E entity) {
        final List<E> list = findAll(userId);
        list.remove(entity);
    }

    @NotNull
    @Override
    public List<E> findAll(@NotNull final String userId) {
        return list.stream()
                .filter(e -> e.getUserId().equals(userId))
                .collect(Collectors.toList());
    }

    @NotNull
    @Override
    public List<E> findAll(@NotNull final String userId, @NotNull Comparator<E> comparator) {
        return list.stream()
                .filter(e -> e.getUserId().equals(userId))
                .sorted(comparator)
                .collect(Collectors.toList());
    }

    @Override
    public E findById(@NotNull final String userId, @NotNull final String id) {
        return findAll(userId).stream()
                .filter(e -> e.getId().equals(id))
                .findFirst().orElse(null);
    }

    @Override
    public E removeById(@NotNull final String userId, @NotNull final String id) {
        final Optional<E> entity =  Optional.ofNullable(findById(userId, id));
        entity.ifPresent(this::remove);
        return entity.orElse(null);
    }

    @NotNull
    @Override
    public Integer getSize(@NotNull final String userId) {
        return list.size();
    }

    @Override
    public E findByName(@NotNull final String userId, @NotNull final String name) {
        return findAll(userId).stream()
                .filter(e -> e.getName().equals(name))
                .findFirst()
                .orElseThrow(EntityNotFoundException::new);
    }

    @Override
    public E findByIndex(@NotNull final String userId, @NotNull final int index) {
        return findAll(userId).get(index);
    }

    @Override
    public void clear(@NotNull final String userId) {
        List<String> entityUserId = list.stream()
                .filter(e -> e.getUserId().equals(userId))
                .map(E::getId)
                .collect(Collectors.toList());
        entityUserId.forEach(list::remove);
    }

}
