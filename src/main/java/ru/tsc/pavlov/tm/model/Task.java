package ru.tsc.pavlov.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.pavlov.tm.api.entity.IWBS;
import ru.tsc.pavlov.tm.enumerated.Status;

import java.util.Date;
import java.util.UUID;

@Getter
@Setter
public final class Task extends AbstractOwnerEntity implements IWBS {

    @NotNull private Status status = Status.NOT_STARTED;

    @NotNull private String name;

    @NotNull private String description;

    @Nullable private String projectId = null;

    @Nullable private Date startDate;

    @NotNull private Date created = new Date();

    @Nullable private String userId;

    public Task() {

    }

    public Task(String name, String description) {
        this.name = name;
        this.description = description;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    @NotNull
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @NotNull
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @NotNull
    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    @Nullable
    public String getProjectId() {
        return projectId;
    }

    @Override
    public String toString() {
        return id + ": " + name;
    }

    @Override
    public Date getCreated() {
        return created;
    }

    @Override
    public void setCreated(Date created) {
        this.created = created;
    }

    @Override
    public Date getStartDate() {
        return startDate;
    }

    @Override
    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

}
