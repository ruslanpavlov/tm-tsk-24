package ru.tsc.pavlov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.pavlov.tm.enumerated.Status;
import ru.tsc.pavlov.tm.model.Task;

public interface ITaskService extends IOwnerService<Task> {

    void create(@Nullable String userId, @Nullable String name);

    void create(@Nullable String userId, @Nullable String name, @Nullable String description);

    Task findByName(@Nullable String userId, @Nullable String name);

    Task findByIndex(@Nullable String userId, @Nullable Integer index);

    @NotNull
    Task removeByName(@Nullable String userId, @Nullable String name);

    Task removeByIndex(@Nullable String userId, @Nullable Integer index);

    void updateById(@NotNull String userId, @Nullable final String id, @Nullable final String name, @Nullable final String index);

    void updateByIndex(@NotNull String userId, @Nullable final Integer index, @Nullable final String name, @Nullable final String description);

    boolean existsByIndex(@Nullable String userId, @NotNull int index);

    boolean existsByName(@Nullable String userId, @NotNull String name);

    Task startById(@Nullable String userId, @Nullable String id);

    Task startByIndex(@Nullable String userId, @Nullable Integer index);

    Task startByName(@Nullable String userId, @Nullable String name);

    Task finishById(@Nullable String userId, @Nullable String id);

    Task finishByIndex(@Nullable String userId, @Nullable Integer index);

    Task finishByName(@Nullable String userId, @Nullable String name);

    Task changeStatusById(@Nullable String userId, @Nullable String id, @Nullable Status status);

    Task changeStatusByIndex(@Nullable String userId, @Nullable Integer index, @Nullable Status status);

    Task changeStatusByName(@Nullable String userId, @Nullable String name, @Nullable Status status);

}
