package ru.tsc.pavlov.tm.enumerated;

import org.jetbrains.annotations.NotNull;
import ru.tsc.pavlov.tm.comparator.ComparatorByCreated;
import ru.tsc.pavlov.tm.comparator.ComparatorByName;
import ru.tsc.pavlov.tm.comparator.ComparatorByStartDate;
import ru.tsc.pavlov.tm.comparator.ComparatorByStatus;

import java.util.Comparator;

public enum Sort {

    NAME("Sort by name",ComparatorByName.getInstance()),
    CREATED("Sort by created",ComparatorByCreated.getInstance()),
    START_DATE("Sort by start date",ComparatorByStartDate.getInstance()),
    STATUS("Sort by status",ComparatorByStatus.getInstance());

    private final String displayName;

    @NotNull
    private final Comparator comparator;

    @NotNull
    public String getDisplayName() {
        return displayName;
    }

    @NotNull
    public Comparator getComparator() {
        return comparator;
    }

    Sort(@NotNull String displayName, @NotNull Comparator comparator) {
        this.displayName = displayName;
        this.comparator = comparator;
    }

}
